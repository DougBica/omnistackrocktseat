const Dev = require('../models/Dev');
const ParseStringAsArray = require('../utils/ParseStringAsArray');


module.exports = {
  async index(request, response){
    // Buscar todos os devs em um raio de 10Km
    // Filtrar por tecnologia
    const {latitude, longitude, techs} = request.query;
    
    const techsArray = ParseStringAsArray(techs);

    const devs = await Dev.find({
      techs:{
        $in: techsArray,
      },
      location: {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: [latitude, longitude],
          },
          $maxDistance: 10000,
        },
      },
    }) 
    return response.json(devs);

  }
}