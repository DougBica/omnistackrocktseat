const axios = require('axios');
const Dev = require('../models/Dev');
const ParseStringAsArray = require('../utils/ParseStringAsArray');
const { findConnections, sendMessage } = require('../websocket');

//geralmente possui 5 function : index, show, store, update, destroy
// index: listar
// show: mostrar 1
// store: salvar
// update: alterar
// destroy: deletar

module.exports = {

  async index(request, response){
    const devs = await Dev.find();

    return response.json(devs);
  },
  async store(request, response){
    const { github_username, techs, longitude, latitude} = request.body;

    let dev = await Dev.findOne({ github_username });

    if(!dev){     
      const apiResponse = await axios.get(`https://api.github.com/users/${github_username}`);
      const { name = login, avatar_url, bio } = apiResponse.data;
    
      const techsArray = ParseStringAsArray(techs);
      const location = {
        type:'Point',
        coordinates:[latitude, longitude],
      }
      dev = await Dev.create({
        github_username,
        name,
        avatar_url,
        bio,
        techs: techsArray,
        location,
      });

      //Filtrar as conexões do websocket que estão à 10km de distância 
      //e que o dev em cadastro tenha uma das tecnologias filtradas

      const sendSocketMessageTo = findConnections( 
        { latitude, longitude },
        techsArray 
      )
      sendMessage(sendSocketMessageTo,'new-dev', dev);
    }
    return response.json(dev);
  }
}