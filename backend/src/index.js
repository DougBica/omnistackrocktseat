const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');
const routes = require('./routes');
const { setupWebsocket } = require('./websocket');


const app = express();
const server = http.Server(app);
setupWebsocket(server);


mongoose.connect('mongodb://usupadrao:usupadrao@cluster0-shard-00-00-7veo4.mongodb.net:27017,cluster0-shard-00-01-7veo4.mongodb.net:27017,cluster0-shard-00-02-7veo4.mongodb.net:27017/week10?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority',
{useNewUrlParser: true, useUnifiedTopology: true});

app.use(cors());
app.use(express.json());
app.use(routes);

//Metodos:
//principais metodos : GET, POST, PUT, DELETE
// GET: Pegar informação
// POST: Criar informação
// PUT: Editor informação
// DELETE: Deletar informação

//Tipos de parâmetros

//Query params:
// Visíveis pela url; request.query(Filtros, ordenação, paginação, ...)
//Route params:
// Fica na rota; request.params(identificar um recurso na alteração ou remoção)
//Body: 
// Corpo da requisição, dados de criação etc...; request.body(Dados para criação/alteração de registros);


//MongoDB (Não-relacional)


server.listen(3333);