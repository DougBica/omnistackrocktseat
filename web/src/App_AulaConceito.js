import React, { useState} from 'react';
// import logo from './logo.svg';
// import './App.css';
// import Header from './Header';


//Componente: Função que retorna conteúdo de interface
//Bloco isolado de HTML, CSS e JS, o qual não interfere no restante da aplicação
//Criar novo: Repetir mesmo código, isolar um pedaço sem afetar o resto.
//Um componente por arquivo.

//Estado:Informações mantidas pelo componente (Lembrar do conceito de Imutabilidade)

//Propriedade: Informações que componente PAI passa para o componente FILHO
//Mesmo que atributo para o HTML (title por exemplo)

function App() {
  const [counter, setCounter] = useState(0);

  function incrementCounter(){
    setCounter(counter + 1);
  }
  return (
    <>
      {/* <Header title="Dashboard"/>
      <Header title="Dashboard 2 "/>
      <Header title="Dashboard 4 "/> */}

    <h1>Contador: {counter}</h1>
      <button onClick={incrementCounter}>Incrementar</button>
    </>
  );
}

export default App;
